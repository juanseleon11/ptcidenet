# Prueba Tecnica CIDENET.

## Instructivo de Ejecución.
1. Dirijasé a la carpeta raiz del projecto.
2. Ejecute mvn spring-boot:run.

Es necesario tener instalado Maven antes de poder ejecutar el proyecto. De igual manera, podría requerirse eliminar la subcarpeta db generado por Spring en caso de error por corrupción. 

## Decisiones importantes y supuestos.
* Las entidades Area y Pais pueden/deben ser consultadas independientemente de su relacion con el cliente. 
* La identificación y el tipo de identificacion del empleado no podran ser cambiadas.
* Se implementa la funcionalidad de eliminar empleado asi no se mencione explicitamente.

## Video
Link :  https://youtu.be/vXmYMeO7XGM

## Información Adicional
* Crear Empleado: http://localhost:8080/employees/create
* Editar Empleado: http://localhost:8080/employees/edit
* Borrar Empleado: http://localhost:8080/employees/delete, utilizando identificacion y tipo de identificacion
* Buscar Empleado http://localhost:8080/employees/search/{numeroPagina}/{CantidadPorPagina}
* Base de Datos: http://localhost:8080/h2/
