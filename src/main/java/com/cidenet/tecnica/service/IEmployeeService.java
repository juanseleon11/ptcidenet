package com.cidenet.tecnica.service;

import java.util.Map;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.cidenet.tecnica.model.Employee;
import com.cidenet.tecnica.rest.dto.DeleteEmployeeDTO;

public interface IEmployeeService {

	Employee createNewEmployee(Employee receivedEmployee);

	Employee updateEmployee(Employee receivedEmployee);

	Page<Employee> searchEmployees(Pageable pageRequest,Map<String, String> queryParams);
	
	boolean deleteEmployee(DeleteEmployeeDTO deleteEmp);

}
