package com.cidenet.tecnica.service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.time.temporal.ChronoUnit;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import org.springframework.stereotype.Service;

import org.springframework.data.domain.Example;
import com.cidenet.tecnica.model.Country;
import com.cidenet.tecnica.model.Employee;
import com.cidenet.tecnica.model.IdentificationType;
import com.cidenet.tecnica.repository.CountryRepository;
import com.cidenet.tecnica.repository.EmployeeRepository;
import com.cidenet.tecnica.rest.dto.DeleteEmployeeDTO;
import com.cidenet.tecnica.rest.exceptions.BadEmployeeSearchException;
import com.cidenet.tecnica.rest.exceptions.BadQueryParameterEmployeeSearchException;
import com.cidenet.tecnica.rest.exceptions.EmployeeAlreadyExistsException;
import com.cidenet.tecnica.rest.exceptions.InvalidInformationException;
import com.cidenet.tecnica.rest.exceptions.NonExistentEmployeeException;


@Service
public class EmployeeService implements IEmployeeService {

	private Logger logger = LoggerFactory.getLogger(getClass());
	private Validator validator;
	private EmployeeRepository employeeRepo;
	private CountryRepository countryRepo;
	@Autowired
	public EmployeeService(EmployeeRepository employeeRepo, Validator validator, CountryRepository countryRepo) {
		super();
		this.employeeRepo = employeeRepo;
		this.validator = validator;
		this.countryRepo=countryRepo;
	}

	@Override
	public Employee createNewEmployee(Employee receivedEmployee) {
		Optional<Employee> optEmployee = employeeRepo.findByIdentificationAndIdentificacionType(receivedEmployee.getIdentification(), receivedEmployee.getIdentificacionType());
		if(optEmployee.isPresent()) {
			String log =String.format("Employee with ID: %s %s already Exists", receivedEmployee.getIdentificacionType().getFullName(), receivedEmployee.getIdentification());
			logger.error(log);
			throw new EmployeeAlreadyExistsException(log);
		}
		receivedEmployee.setStatus("Activo");
		checkEmployeeConditions(receivedEmployee, false);
		generateEmployeeEmail(receivedEmployee);
		return employeeRepo.save(receivedEmployee);
	}

	public void setValidator(Validator validator) {
		this.validator = validator;
	}

	@Override
	public Employee updateEmployee(Employee receivedEmployee) {
		
		Optional<Employee> optEmployee = employeeRepo.findByIdentificationAndIdentificacionType(receivedEmployee.getIdentification(), receivedEmployee.getIdentificacionType());
		Employee employee;
		logger.debug("Checking if employee exists");
		if (optEmployee.isPresent()) {
			logger.debug("Employee exists");
			employee = optEmployee.get();
		}else {
			employee = receivedEmployee;
		}
		receivedEmployee.setStatus("Activo");
		checkEmployeeConditions(receivedEmployee, true);
		logger.debug("Updating email.");
		generateEmployeeEmail(receivedEmployee);
		mergeEmployees(employee, receivedEmployee);
		return employeeRepo.save(employee);
	}

	@Override
	public Page<Employee> searchEmployees(Pageable pageRequest, Map<String, String> queryParams) {
		Employee example = new Employee();
		boolean error = false;
		boolean badQuery = false;
		String badParam = "";
		if(queryParams.isEmpty()) {
			return employeeRepo.findAll(pageRequest);
		}else {
			for(String params : queryParams.keySet()) {
				String value = queryParams.get(params);
				switch(params) {
				case "identification":
					example.setIdentification(value);
					break;
				case "firstLastName":
					example.setFirstLastName(value);
					break;
				case "secondLastName":
					example.setSecondLastName(value);
					break;
				case "name":
					example.setName(value);
					break;
				case "aditionalNames":
					example.setAditionalNames(value);
					break;
				case "identificacionType":
					try {
						example.setIdentificacionType(Enum.valueOf(IdentificationType.class, value));
					}catch(Exception e) {
						error = true;
					}
					break;
				case "country":
					Optional<Country> optcount = countryRepo.findByName(value);
					if(optcount.isPresent()) {
						example.setCountry(optcount.get());
					}else {
						error = true;
					}
					
					break;
				case "email":
					example.setEmail(value);
					break;
				case "status":
					example.setStatus(value);
					break;
				default:
					badQuery = true;
					badParam = value;
				}
				
			}
			if(badQuery) {
				throw new BadQueryParameterEmployeeSearchException(badParam);
			}
			
			if(error) {	
				StringBuffer buffer = new StringBuffer();
				for(String params : queryParams.keySet()) {
					buffer.append(String.format("%s:%s",params, queryParams.get(params)));
				}
				logger.error("Bad Query Detected: "+ buffer);
				throw new BadEmployeeSearchException(buffer);
			}
			
			return employeeRepo.findAll(Example.of(example),pageRequest);
		}
		
	}

	private void checkEmployeeConditions(Employee employee, boolean isUpdate) {
		logger.debug("Checking Employee information Conditions");
		Set<ConstraintViolation<Employee>> violations = validator.validate(employee);
		LocalDate start = employee.getStartDate();
		LocalDateTime now = LocalDateTime.now();
		LocalDate nowDay = now.toLocalDate();
		long days = ChronoUnit.DAYS.between(start, nowDay);
		logger.debug("There are " + days + " days between today and employee's start day.");
		if (!isUpdate) {
			
			logger.debug("We are a creating a new employee");
			
			if (days > 31) {
				String log = "Start date (%s) is more than a month (%d days) away from today.";
				logger.error(String.format(log, start.toString(), days));
				throw new InvalidInformationException(String.format(log, start.toString(), days));
			} else if (start.isAfter(nowDay)) {
				String log = "Start date (%s) is is greater than today's date.";
				logger.error(String.format(log, start.toString()));
				throw new InvalidInformationException(String.format(log, start.toString()));
			}
		}

		if (!violations.isEmpty()) {
			String log = "There are %d violations for the employee";
			logger.error(String.format(log, violations.size()));
			throw new ConstraintViolationException(violations);
		}

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
		employee.setRegistryDate(formatter.format(now));
	}

	private void generateEmployeeEmail(Employee employee) {

		Optional<List<Employee>> empWithSameEmail = employeeRepo.findByNameAndFirstLastNameAndCountry_id(employee.getName(),
				employee.getFirstLastName(),employee.getCountry().getId());
		logger.debug(String.format("Generating employee's email with name %s and last name %s", employee.getName(),employee.getFirstLastName()));
		StringBuffer buffer = new StringBuffer();
		buffer.append(employee.getName());
		buffer.append(".");
		buffer.append(employee.getFirstLastName().replaceAll("\\s+", ""));
		empWithSameEmail.ifPresent(empList -> {
			if (empList.size() > 0) {
				buffer.append(".");
				buffer.append(empList.size());
			}
		});

		buffer.append("@");
		buffer.append(employee.getCountry().getEmailDomain());
		employee.setEmail(buffer.toString().toLowerCase());
		logger.debug("Generated Email is: "+buffer.toString().toLowerCase());
	}

	private void mergeEmployees(Employee origin, Employee newEmployee) {
		origin.setName(newEmployee.getName());
		origin.setFirstLastName(newEmployee.getFirstLastName());
		origin.setSecondLastName(newEmployee.getSecondLastName());
		origin.setAditionalNames(newEmployee.getAditionalNames());
		origin.setCountry(newEmployee.getCountry());
		origin.setIdentificacionType(newEmployee.getIdentificacionType());
		origin.setEmail(newEmployee.getEmail());
		origin.setArea(newEmployee.getArea());
		origin.setStartDate(newEmployee.getStartDate());
	}

	@Override
	public boolean deleteEmployee(DeleteEmployeeDTO deleteEmp) {
		logger.debug("Deleting an Employee with info " + deleteEmp.toString());
		Optional<Employee> emp = employeeRepo.findByIdentificationAndIdentificacionType(deleteEmp.getIdentification(), deleteEmp.getIdentificacionType());
		if (emp.isPresent()) {
			logger.debug("Found employee... deleting");
			Employee toDelete = emp.get();
			employeeRepo.delete(toDelete);
			return true;
		}
			throw new NonExistentEmployeeException(deleteEmp);
	}

}
