package com.cidenet.tecnica.service;

import java.util.List;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.cidenet.tecnica.model.Area;
import com.cidenet.tecnica.repository.AreaRepository;
import com.cidenet.tecnica.rest.exceptions.NoAreasException;
import com.cidenet.tecnica.rest.exceptions.NonExistentAreaException;


@Component
@Service
public class AreaService implements IAreaService {
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	private AreaRepository areaRepo;

	@Autowired
	public AreaService(AreaRepository areaRepo) {
		super();
		this.areaRepo = areaRepo;
	}

	@Override
	public Area searchArea(String name) {

		Optional<Area> area = areaRepo.findByName(name);
		Area retArea = null;
		logger.debug("Searching Area with name "+name);
		if (area.isPresent()) {
			logger.debug("Found area");
			retArea = area.get();
		} else {
			logger.error("Couldn't Found area");
			throw new NonExistentAreaException(name);
		}
		return retArea;
	}

	@Override
	public List<Area> getAllAreas() {
		List <Area> areas = areaRepo.findAll();
		if(areas.isEmpty()) {
			logger.error("There are no areas availible");
			throw new NoAreasException();
		}
		return areas;
	}

}
