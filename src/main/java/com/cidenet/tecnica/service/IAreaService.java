package com.cidenet.tecnica.service;

import java.util.List;

import com.cidenet.tecnica.model.Area;

public interface IAreaService {
	
	Area searchArea(String name);
	
	List<Area> getAllAreas();

}
