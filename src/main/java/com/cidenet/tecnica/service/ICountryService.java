package com.cidenet.tecnica.service;

import java.util.List;

import com.cidenet.tecnica.model.Country;

public interface ICountryService {
	
	Country searchCountry(String name);
	List<Country> getAllCountries();

}
