package com.cidenet.tecnica.service;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cidenet.tecnica.model.Country;
import com.cidenet.tecnica.repository.CountryRepository;
import com.cidenet.tecnica.rest.exceptions.NoCountriesException;
import com.cidenet.tecnica.rest.exceptions.NonExistentCountryException;

@Service
public class CountryService implements ICountryService {
	private Logger logger = LoggerFactory.getLogger(getClass());
	private CountryRepository countryRepo;
	
	@Autowired
	public CountryService(CountryRepository countryRepo) {
		super();
		this.countryRepo = countryRepo;
	}

	@Override
	public Country searchCountry(String name) {

		Optional<Country> country = countryRepo.findByName(name);
		
		Country retCount = null;
		logger.debug("Searching Country with name "+name);
		if (country.isPresent()) {
			logger.debug("Found Country");
			retCount = country.get();
		} else {
			logger.debug("Couldn't Found Country");
			throw new NonExistentCountryException(name);
		}
		return retCount;
	}



	@Override
	public List<Country> getAllCountries() {
		List <Country> countries = countryRepo.findAll();
		if(countries.isEmpty()) {
			logger.error("There are no countries availible");
			throw new NoCountriesException();
		}
		return countries;
	}

}
