package com.cidenet.tecnica.rest;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cidenet.tecnica.model.Country;
import com.cidenet.tecnica.rest.dto.CountryDTO;
import com.cidenet.tecnica.service.ICountryService;

@RestController
@RequestMapping("countries")
public class CountryController {
	
	@Autowired
	private ICountryService countryServ;
	
	Logger logger = LoggerFactory.getLogger(getClass());
	
	@GetMapping("/getAll")
	public List<CountryDTO> getAllCountries() {
		logger.debug("Searching for all Countries");
		ModelMapper mapper=new ModelMapper();
		List<Country> areas = countryServ.getAllCountries();
		List<CountryDTO> countriesDTO = new ArrayList<>();
		areas.forEach(area->{
			logger.debug("Mapping DTO to Class"+area.toString());
			countriesDTO.add(mapper.map(area, CountryDTO.class));
		});

		return countriesDTO;
	}

	
}