package com.cidenet.tecnica.rest;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cidenet.tecnica.model.Area;
import com.cidenet.tecnica.rest.dto.AreaDTO;
import com.cidenet.tecnica.service.IAreaService;

@RestController
@RequestMapping("areas")
public class AreaController {

	@Autowired
	private IAreaService areaServ;
	
	Logger logger = LoggerFactory.getLogger(getClass());
	
	@GetMapping("/getAll")
	public List<AreaDTO> getAllAreas() {
		logger.debug("Searching for all Areas");
		ModelMapper mapper=new ModelMapper();
		List<Area> areas = areaServ.getAllAreas();
		List<AreaDTO> areasDTO = new ArrayList<>();
		areas.forEach(area->{
			logger.debug("Mapping DTO to Class"+area.toString());
			areasDTO.add(mapper.map(area, AreaDTO.class));
		});

		return areasDTO;
	}	
}