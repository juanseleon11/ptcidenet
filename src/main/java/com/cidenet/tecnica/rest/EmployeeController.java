package com.cidenet.tecnica.rest;

import java.util.Map;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cidenet.tecnica.model.Area;
import com.cidenet.tecnica.model.Country;
import com.cidenet.tecnica.model.Employee;
import com.cidenet.tecnica.rest.dto.DeleteEmployeeDTO;
import com.cidenet.tecnica.rest.dto.EmployeeDTO;
import com.cidenet.tecnica.service.IAreaService;
import com.cidenet.tecnica.service.ICountryService;
import com.cidenet.tecnica.service.IEmployeeService;

@RestController
@RequestMapping("employees")
public class EmployeeController {

	@Autowired
	private IEmployeeService employeeServ;
	
	@Autowired
	private ICountryService countryServ;
	
	@Autowired
	private IAreaService areaServ;
	
	Logger logger = LoggerFactory.getLogger(getClass());
	
	@PostMapping("/create")
	public EmployeeDTO createNewEmployee(@RequestBody EmployeeDTO empleado) {
		logger.debug("Creating new Employee");
		System.out.println(empleado);
		Country country = countryServ.searchCountry(empleado.getCountryStr());
		Area area = areaServ.searchArea(empleado.getAreaStr());
		ModelMapper mapper=new ModelMapper();
		logger.debug("Mapping DTO to Class"+empleado.toString());
		Employee receivedEmployee = mapper.map(empleado, Employee.class);
		receivedEmployee.setCountry(country);
		receivedEmployee.setArea(area);
		Employee emp = employeeServ.createNewEmployee(receivedEmployee);
		EmployeeDTO dto = mapper.map(emp, EmployeeDTO.class);
		dto.setAreaStr(emp.getArea().getName());
		dto.setCountryStr(emp.getCountry().getName());
		return dto;
	}
	
	@PutMapping("/edit")
	public EmployeeDTO editEmployee(@RequestBody EmployeeDTO empleado) {
		logger.debug("Updating an Employee");
		Country country = countryServ.searchCountry(empleado.getCountryStr());
		Area area = areaServ.searchArea(empleado.getAreaStr());
		ModelMapper mapper=new ModelMapper();
		logger.debug("Mapping DTO to Class"+empleado.toString());
		Employee receivedEmployee = mapper.map(empleado, Employee.class);
		receivedEmployee.setCountry(country);
		receivedEmployee.setArea(area);
		Employee emp = employeeServ.updateEmployee(receivedEmployee);
		EmployeeDTO dto = mapper.map(emp, EmployeeDTO.class);
		dto.setAreaStr(emp.getArea().getName());
		dto.setCountryStr(emp.getCountry().getName());
		return dto;
	}
	
	@GetMapping("/search/{pag}/{pageSize}")
	public Page<EmployeeDTO> searchEmployees(@PathVariable("pag") int pag, 
			@PathVariable("pageSize")int pageSize, 
			@RequestParam(required=false) Map<String,String> queryParams) {
		ModelMapper mapper=new ModelMapper();
		int size = Integer.valueOf(pageSize);
		logger.debug(String.format("Searching Employees in sized %d pages. Looking for page %d", size,pag));
		
		
		Pageable pageRequest = PageRequest.of(pag, pageSize);
		Page<Employee> retPage = employeeServ.searchEmployees(pageRequest, queryParams);
		Page<EmployeeDTO> dtos = retPage.map(entity->{
			EmployeeDTO empDTO = mapper.map(entity, EmployeeDTO.class); 
			empDTO.setAreaStr(entity.getArea().getName());
			empDTO.setCountryStr(entity.getCountry().getName());
			return empDTO;
		});

		return dtos;
	}
	
	@DeleteMapping("delete")
	public boolean deleteEmployee(DeleteEmployeeDTO dto) {
		logger.debug("Deleting an Employee");
		return employeeServ.deleteEmployee(dto);
	}
	
}