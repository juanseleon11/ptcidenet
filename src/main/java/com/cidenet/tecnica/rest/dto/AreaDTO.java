package com.cidenet.tecnica.rest.dto;


public class AreaDTO {

	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	
}
