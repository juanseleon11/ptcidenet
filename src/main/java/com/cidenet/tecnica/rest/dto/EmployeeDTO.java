package com.cidenet.tecnica.rest.dto;

import java.time.LocalDate;
import java.time.LocalDateTime;

import com.cidenet.tecnica.model.IdentificationType;

public class EmployeeDTO {

	private String identification;
	
	private String firstLastName;
	
	private String secondLastName;
	
	private String name;
	
	private String aditionalNames;
	
	private String countryStr;
	
	private IdentificationType identificacionType;
	
	private String email;
	
	private String areaStr;
	
	private LocalDate startDate;

	private String status;
	

	public String getIdentification() {
		return identification;
	}

	public void setIdentification(String identification) {
		this.identification = identification;
	}

	public String getFirstLastName() {
		return firstLastName;
	}

	public void setFirstLastName(String firstLastName) {
		this.firstLastName = firstLastName;
	}

	public String getSecondLastName() {
		return secondLastName;
	}

	public void setSecondLastName(String secondLastName) {
		this.secondLastName = secondLastName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAditionalNames() {
		return aditionalNames;
	}

	public void setAditionalNames(String aditionalNames) {
		this.aditionalNames = aditionalNames;
	}

	public String getCountryStr() {
		return countryStr;
	}

	public void setCountryStr(String country) {
		this.countryStr = country;
	}

	public IdentificationType getIdentificacionType() {
		return identificacionType;
	}

	public void setIdentificacionType(IdentificationType identificacionType) {
		this.identificacionType = identificacionType;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAreaStr() {
		return areaStr;
	}

	public void setAreaStr(String area) {
		this.areaStr = area;
	}

	public LocalDate getStartDate() {
		return startDate;
	}

	public void setStartDate(LocalDate startDate) {
		this.startDate = startDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "EmployeeDTO [identification=" + identification + ", firstLastName=" + firstLastName
				+ ", secondLastName=" + secondLastName + ", name=" + name + ", aditionalNames=" + aditionalNames
				+ ", countryStr=" + countryStr + ", identificacionType=" + identificacionType + ", email=" + email
				+ ", areaStr=" + areaStr + ", startDate=" + startDate + ", status=" + status + "]";
	}

	
	
}
