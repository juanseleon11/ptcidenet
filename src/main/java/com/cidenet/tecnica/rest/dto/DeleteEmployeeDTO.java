package com.cidenet.tecnica.rest.dto;

import com.cidenet.tecnica.model.IdentificationType;

public class DeleteEmployeeDTO {
	private String identification;
	
	private IdentificationType identificacionType;

	public String getIdentification() {
		return identification;
	}

	public void setIdentification(String identification) {
		this.identification = identification;
	}

	public IdentificationType getIdentificacionType() {
		return identificacionType;
	}

	public void setIdentificacionType(IdentificationType identificacionType) {
		this.identificacionType = identificacionType;
	}
	
	
}
