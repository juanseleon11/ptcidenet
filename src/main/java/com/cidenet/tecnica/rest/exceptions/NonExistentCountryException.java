package com.cidenet.tecnica.rest.exceptions;

public class NonExistentCountryException extends RuntimeException{

	public NonExistentCountryException(String name) {
		super(String.format("Country with name %s does not exists.", name));
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
