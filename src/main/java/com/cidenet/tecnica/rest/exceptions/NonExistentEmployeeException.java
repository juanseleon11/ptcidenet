package com.cidenet.tecnica.rest.exceptions;

import com.cidenet.tecnica.rest.dto.DeleteEmployeeDTO;

public class NonExistentEmployeeException extends RuntimeException{

	public NonExistentEmployeeException(DeleteEmployeeDTO receivedEmployee) {
		super(String.format("Employee with identification %s %s does not exists.", receivedEmployee.getIdentificacionType(),receivedEmployee.getIdentification()));
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
