package com.cidenet.tecnica.rest.exceptions;

public class NoAreasException extends RuntimeException{

	public NoAreasException() {
		super("None areas were found.");
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
