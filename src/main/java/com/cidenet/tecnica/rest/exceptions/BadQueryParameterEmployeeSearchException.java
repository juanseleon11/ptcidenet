package com.cidenet.tecnica.rest.exceptions;

public class BadQueryParameterEmployeeSearchException extends RuntimeException {

	public BadQueryParameterEmployeeSearchException(String value) {
		super(String.format("Param %s is not a valid parameter for employee search", value));
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -6609325745408748379L;

}
