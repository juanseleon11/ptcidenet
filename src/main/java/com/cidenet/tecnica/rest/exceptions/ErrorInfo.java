package com.cidenet.tecnica.rest.exceptions;

public class ErrorInfo {
	
    public final String code;

    public ErrorInfo( Exception ex) {
        this.code = ex.getMessage();
    }

}