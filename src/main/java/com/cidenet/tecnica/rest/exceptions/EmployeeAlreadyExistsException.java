package com.cidenet.tecnica.rest.exceptions;


public class EmployeeAlreadyExistsException extends RuntimeException{

	public EmployeeAlreadyExistsException(String log) {
		super(log);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
