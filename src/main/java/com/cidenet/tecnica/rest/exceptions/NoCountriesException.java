package com.cidenet.tecnica.rest.exceptions;

public class NoCountriesException extends RuntimeException{

	public NoCountriesException() {
		super("None countries were found.");
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
