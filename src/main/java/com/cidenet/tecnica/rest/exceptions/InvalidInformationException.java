package com.cidenet.tecnica.rest.exceptions;

public class InvalidInformationException extends RuntimeException{

	public InvalidInformationException(String log) {
		super(log);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
