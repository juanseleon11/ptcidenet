package com.cidenet.tecnica.rest.exceptions;

public class BadEmployeeSearchException extends RuntimeException{

	public BadEmployeeSearchException(StringBuffer buffer) {
		
		super(String.format("Search with params [%s] has errors either in the Country or in the IdentificationType field", buffer));
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
