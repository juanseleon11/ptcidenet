package com.cidenet.tecnica.rest.exceptions;

public class NonExistentAreaException extends RuntimeException{

	public NonExistentAreaException(String name) {
		super(String.format("Area with name %s does not exists.", name));
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
