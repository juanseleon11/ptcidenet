package com.cidenet.tecnica.rest;

import javax.validation.ConstraintViolationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.cidenet.tecnica.rest.exceptions.*;


@ControllerAdvice
public class GlobalControllerAdvice {
	Logger logger = LoggerFactory.getLogger(getClass());
	
	@ResponseBody
	@ExceptionHandler({BadEmployeeSearchException.class})
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	ErrorInfo handleBadEmployeeSearchException(BadEmployeeSearchException e) {
		return new ErrorInfo(e);
	}
	
	@ResponseBody
	@ExceptionHandler({BadQueryParameterEmployeeSearchException.class})
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	ErrorInfo handleBadQueryParameterEmployeeSearchException(BadQueryParameterEmployeeSearchException e) {
		return new ErrorInfo(e);
	}
	
	@ResponseBody
	@ExceptionHandler({EmployeeAlreadyExistsException.class})
	@ResponseStatus(value = HttpStatus.CONFLICT)
	ErrorInfo handleEmployeeAlreadyExistsException(EmployeeAlreadyExistsException e) {
		return new ErrorInfo(e);
	}
	
	@ResponseBody
	@ExceptionHandler({InvalidInformationException.class})
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	ErrorInfo handleInvalidInformationException(InvalidInformationException e) {
		return new ErrorInfo(e);
	}
	
	@ResponseBody
	@ExceptionHandler({NoAreasException.class})
	@ResponseStatus(value = HttpStatus.NOT_FOUND)
	ErrorInfo handleNoAreasException(NoAreasException e) {
		return new ErrorInfo(e);
	}
	
	@ResponseBody
	@ExceptionHandler({NoCountriesException.class})
	@ResponseStatus(value = HttpStatus.NOT_FOUND)
	ErrorInfo handleNoCountriesException(NoCountriesException e) {
		return new ErrorInfo(e);
	}
	
	@ResponseBody
	@ExceptionHandler({NonExistentAreaException.class})
	@ResponseStatus(value = HttpStatus.NOT_FOUND)
	ErrorInfo handleNonExistentCountryException(NonExistentAreaException e) {
		return new ErrorInfo(e);
	}
	

	
	@ResponseBody
	@ExceptionHandler({NonExistentCountryException.class})
	@ResponseStatus(value = HttpStatus.NOT_FOUND)
	ErrorInfo handleNonExistentCountryException(NonExistentCountryException e) {
		return new ErrorInfo(e);
	}
	

	
	@ResponseBody
	@ExceptionHandler({NonExistentEmployeeException.class})
	@ResponseStatus(value = HttpStatus.NOT_FOUND)
	ErrorInfo handleNonExistentException(NonExistentEmployeeException e) {
		return new ErrorInfo(e);
	}
	
	
	@ResponseBody
	@ExceptionHandler({ConstraintViolationException.class})
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	ErrorInfo handleConstraintViolationException(ConstraintViolationException e) {
		return new ErrorInfo(e);
	}
}
