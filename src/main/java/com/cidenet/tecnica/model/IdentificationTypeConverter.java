package com.cidenet.tecnica.model;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import java.util.stream.*;

@Converter(autoApply = true)
public class IdentificationTypeConverter implements AttributeConverter<IdentificationType, String>{

	@Override
	public String convertToDatabaseColumn(IdentificationType attribute) {
		
		return attribute.getFullName();
	}

	@Override
	public IdentificationType convertToEntityAttribute(String tipo) {
		return Stream.of(IdentificationType.values())
		          .filter(tp -> tp.getFullName().equals(tipo))
		          .findFirst()
		          .orElseThrow(IllegalArgumentException::new);
	}

}
