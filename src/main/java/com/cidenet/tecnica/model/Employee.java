package com.cidenet.tecnica.model;

import java.time.LocalDate;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Length;

@Entity
@IdClass(EmployeeId.class)
public class Employee {
	@Id
	@Pattern(regexp = "[A-Za-z0-9\\-]+")
	@Length(min = 1, max = 20)
	@Column(nullable= false,length = 20)
	private String identification;
	
	@Basic
	@Pattern(regexp = "[A-Z\\s]+")
	@Length(min = 1, max = 20)
	@Column(nullable= false,length = 20)
	private String firstLastName;
	
	@Basic
	@Pattern(regexp = "[A-Z\\s]+")
	@Length(min = 1, max = 20)
	@Column(nullable= false,length = 20)
	private String secondLastName;
	
	@Basic
	@Pattern(regexp = "[A-Z]+")
	@Length(min = 1, max = 20)
	@Column(nullable= false,length = 20)
	private String name;
	
	@Basic
	@Pattern(regexp = "[A-Z\\s]+")
	@Length(max = 50)
	@Column(nullable= true,length = 50)
	private String aditionalNames;
	 
	@ManyToOne(optional = false, fetch = FetchType.EAGER)
	private Country country;
	
	@Id
	@Enumerated(EnumType.STRING)
	@Column(nullable= false)
	private IdentificationType identificacionType;
	
	@Basic
	@Length(min = 1, max = 300)
	@Column(nullable= false,length = 300)
	private String email;
	
	@ManyToOne(optional= false, fetch = FetchType.EAGER)
	private Area area;
	
	private LocalDate startDate;

	@Basic
	private String status;
	
	@Basic
	private String registryDate;

	public String getIdentification() {
		return identification;
	}

	public void setIdentification(String identification) {
		this.identification = identification;
	}

	public String getFirstLastName() {
		return firstLastName;
	}

	public void setFirstLastName(String firstLastName) {
		this.firstLastName = firstLastName;
	}

	public String getSecondLastName() {
		return secondLastName;
	}

	public void setSecondLastName(String secondLastName) {
		this.secondLastName = secondLastName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAditionalNames() {
		return aditionalNames;
	}

	public void setAditionalNames(String aditionalNames) {
		this.aditionalNames = aditionalNames;
	}

	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	public IdentificationType getIdentificacionType() {
		return identificacionType;
	}

	public void setIdentificacionType(IdentificationType identificacionType) {
		this.identificacionType = identificacionType;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Area getArea() {
		return area;
	}

	public void setArea(Area area) {
		this.area = area;
	}

	public LocalDate  getStartDate() {
		return startDate;
	}

	public void setStartDate(LocalDate  startDate) {
		this.startDate = startDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getRegistryDate() {
		return registryDate;
	}

	public void setRegistryDate(String registryDate) {
		this.registryDate = registryDate;
	}

	@Override
	public String toString() {
		return "Employee [identification=" + identification + ", firstLastName=" + firstLastName + ", secondLastName="
				+ secondLastName + ", name=" + name + ", aditionalNames=" + aditionalNames + ", country=" + country
				+ ", identificacionType=" + identificacionType + ", email=" + email + ", area=" + area + ", startDate="
				+ startDate + ", status=" + status + ", registryDate=" + registryDate + "]";
	}
	
	

}
