package com.cidenet.tecnica.model;

import java.io.Serializable;

import javax.persistence.Embeddable;

public class EmployeeId implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 538988200293167694L;
	private IdentificationType identificacionType;
	private String identification;
	
	
	public EmployeeId(IdentificationType type, String identification) {
		super();
		this.identificacionType = type;
		this.identification = identification;
	}
	
	public EmployeeId() {

	}
	

}
