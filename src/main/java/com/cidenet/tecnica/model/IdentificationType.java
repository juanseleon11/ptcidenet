package com.cidenet.tecnica.model;

public enum IdentificationType {
CC("Cédula de Ciudadanía"),CE("Cédula de Extranjería"),PASS("Pasaporte"),PE("Permiso Especial");
	 private String tipo;
    private IdentificationType(String tipo) {
        this.tipo = tipo;
    }

    public String getFullName() {
        return tipo;
    }
}
