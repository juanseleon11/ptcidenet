package com.cidenet.tecnica;

import java.time.LocalDate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import com.cidenet.tecnica.model.Area;
import com.cidenet.tecnica.model.Country;
import com.cidenet.tecnica.model.Employee;
import com.cidenet.tecnica.model.IdentificationType;
import com.cidenet.tecnica.repository.AreaRepository;
import com.cidenet.tecnica.repository.CountryRepository;
import com.cidenet.tecnica.repository.EmployeeRepository;


@Configuration
public class TecnicaConfig {
	
	Logger logger = LoggerFactory.getLogger(getClass());
	@Autowired
	Environment env;
	@Autowired
	EmployeeRepository employeeRepo;
	
	@Autowired
	CountryRepository countryRepo;
	
	@Autowired
	AreaRepository areaRepo;
	
	@Bean
	CommandLineRunner initCommands(EmployeeRepository eRepository) {
		return args->{
			logger.debug("Populating DataBase");
			logger.debug("Populating Countries Table");
			Country col= new Country();
	    	col.setName("Colombia");
	    	col.setEmailDomain("cidenet.com.co");
	    	col = countryRepo.save(col);
	    	Country us = new Country();
	        us.setName("USA");
	        us.setEmailDomain("cidenet.com.us");
	        us =countryRepo.save(us);
			logger.debug("Populating Areas Table");
	        
			Area ar1 = new Area();
	        ar1.setName("Administración");
	        ar1 =areaRepo.save(ar1);
			
			Area ar2 = new Area();
			ar2.setName("Financiera");
			ar2= areaRepo.save(ar2);
	        
			Area ar3 = new Area();
			ar3.setName("Compras");
			ar3 = areaRepo.save(ar3);
	        
			Area ar4 = new Area();
			ar4.setName("Infraestructura");
			ar4 = areaRepo.save(ar4);
	        
			Area ar5 = new Area();
			ar5.setName("Operación");
			ar5 = areaRepo.save(ar5);
	        
			Area ar6 = new Area();
			ar6.setName("Talento Humano");
			ar6 = areaRepo.save(ar6);
	        
			Area ar7 = new Area();
			ar7.setName("Servicios Varios");
			ar7 = areaRepo.save(ar7);
			
			logger.debug("Populating Employees Table");
			
	    	Employee employee = new Employee();
	    	employee.setName("PEDRO");
	    	employee.setFirstLastName("RAMIREZ");
	    	employee.setSecondLastName("CORTES");
	    	employee.setAditionalNames("RAMIRO");
	    	employee.setCountry(col);
	    	employee.setIdentificacionType(IdentificationType.CC);
	    	employee.setIdentification("HOla-123");
	    	employee.setArea(ar1);
	    	employee.setStartDate(LocalDate.now().minusDays(10));
	    	employee.setEmail("pedro.ramirez@cidenet.com.co");
	    	employeeRepo.save(employee);
	    	
	    	employee = new Employee();
	    	employee.setName("PEDRO");
	    	employee.setFirstLastName("RAMIREZ");
	    	employee.setSecondLastName("CHAR");
	    	employee.setAditionalNames("MIGUEL");
	    	employee.setCountry(us);
	    	employee.setIdentificacionType(IdentificationType.PASS);
	    	employee.setIdentification("HOla-123");
	    	employee.setArea(ar7);
	    	employee.setStartDate(LocalDate.now().minusDays(1));
	    	employee.setEmail("pedro.ramirez@cidenet.com.us");
	    	employeeRepo.save(employee);
	    	
	    	employee = new Employee();
	    	employee.setName("RAUL");
	    	employee.setFirstLastName("DE LA ROSA");
	    	employee.setSecondLastName("RAMIREZ");
	    	employee.setAditionalNames("HERNANDO");
	    	employee.setCountry(col);
	    	employee.setIdentificacionType(IdentificationType.CC);
	    	employee.setIdentification("123456215");
	    	employee.setArea(ar4);
	    	employee.setStartDate(LocalDate.now().minusDays(4));
	    	employee.setEmail("raul.delarosa@cidenet.com.co");
	    	employeeRepo.save(employee);
	    	
	    	employee = new Employee();
	    	employee.setName("AMPARO");
	    	employee.setFirstLastName("GRISALES");
	    	employee.setSecondLastName("CORTES");
	    	employee.setCountry(col);
	    	employee.setIdentificacionType(IdentificationType.CC);
	    	employee.setIdentification("HOla-123");
	    	employee.setArea(ar5);
	    	employee.setStartDate(LocalDate.now().minusDays(5));
	    	employee.setEmail("amparo.grisales@cidenet.com.co");
	    	employeeRepo.save(employee);
	    	
	    	employee = new Employee();
	    	employee.setName("BRITNEY");
	    	employee.setFirstLastName("SPEARS");
	    	employee.setSecondLastName("SPEARS");

	    	employee.setCountry(us);
	    	employee.setIdentificacionType(IdentificationType.PASS);
	    	employee.setIdentification("HiT-ME-B4by");
	    	employee.setArea(ar6);
	    	employee.setStartDate(LocalDate.now().minusDays(20));
	    	employee.setEmail("britney.spears@cidenet.com.us");
	    	employeeRepo.save(employee);
	    	
	    	employee = new Employee();
	    	employee.setName("MARBELLE");
	    	employee.setFirstLastName("GOMEZ");
	    	employee.setSecondLastName("MEBARAK");
	    	employee.setCountry(col);
	    	employee.setIdentificacionType(IdentificationType.CE);
	    	employee.setIdentification("Perlas");
	    	employee.setArea(ar7);
	    	employee.setStartDate(LocalDate.now());
	    	employee.setEmail("marbelle.gomez@cidenet.com.co");
	    	employeeRepo.save(employee);
	    	
	    	employee = new Employee();
	    	employee.setName("PETER");
	    	employee.setFirstLastName("SMITH");
	    	employee.setSecondLastName("VOLTA");
	    	employee.setCountry(us);
	    	employee.setIdentificacionType(IdentificationType.CC);
	    	employee.setIdentification("GREAT");
	    	employee.setArea(ar1);
	    	employee.setStartDate(LocalDate.now());
	    	employee.setEmail("peter.smith@cidenet.com.us");
	    	employeeRepo.save(employee);
	    	
	    	employee = new Employee();
	    	employee.setName("ANDRES");
	    	employee.setFirstLastName("RAMIREZ");
	    	employee.setSecondLastName("CORTES");
	    	employee.setAditionalNames("RAMIRO");
	    	employee.setCountry(col);
	    	employee.setIdentificacionType(IdentificationType.CC);
	    	employee.setIdentification("HOla-1234");
	    	employee.setArea(ar6);
	    	employee.setStartDate(LocalDate.now());
	    	employee.setEmail("andres.ramirez@cidenet.com.co");
	    	employeeRepo.save(employee);
	    	
	    	employee = new Employee();
	    	employee.setName("MARGARITA");
	    	employee.setFirstLastName("RAMIREZ");
	    	employee.setSecondLastName("CORTES");
	    	employee.setAditionalNames("FLOR");
	    	employee.setCountry(col);
	    	employee.setIdentificacionType(IdentificationType.CC);
	    	employee.setIdentification("MARGA");
	    	employee.setArea(ar4);
	    	employee.setStartDate(LocalDate.now());
	    	employee.setEmail("margarita.ramirez@cidenet.com.co");
	    	employeeRepo.save(employee);
	    	
	    	employee = new Employee();
	    	employee.setName("JUAN");
	    	employee.setFirstLastName("LEON");
	    	employee.setSecondLastName("SUAREZ");
	    	employee.setAditionalNames("SEBASTIAN");
	    	employee.setCountry(col);
	    	employee.setIdentificacionType(IdentificationType.CC);
	    	employee.setIdentification("868542a");
	    	employee.setArea(ar1);
	    	employee.setStartDate(LocalDate.now());
	    	employee.setEmail("juan.leon@cidenet.com.co");
	    	employeeRepo.save(employee);    	
		};
	}

}
