package com.cidenet.tecnica.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cidenet.tecnica.model.Area;

public interface AreaRepository extends JpaRepository<Area, Long>{

	Optional<Area> findByName(String name);
}
