package com.cidenet.tecnica.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cidenet.tecnica.model.Country;

public interface CountryRepository extends JpaRepository<Country, Long>{

	Optional<Country> findByName(String name);
}
