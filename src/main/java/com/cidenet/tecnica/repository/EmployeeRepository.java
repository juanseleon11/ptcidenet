package com.cidenet.tecnica.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cidenet.tecnica.model.Employee;
import com.cidenet.tecnica.model.IdentificationType;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, String>{
	
	Optional<Employee> findByIdentificationAndIdentificacionType(String identification, IdentificationType identificacionType);
	Optional<List<Employee>> findByNameAndFirstLastNameAndCountry_id(String name, String firstLastName, int id);
}
