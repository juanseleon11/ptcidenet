package com.cidenet.tecnica;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableAutoConfiguration(exclude = { SecurityAutoConfiguration.class})
@ComponentScan("com.cidenet.tecnica")
@EntityScan("com.cidenet.tecnica")
@EnableJpaRepositories("com.cidenet.tecnica")

public class TecnicaApplication {
	public static void main(String[] args) {
		SpringApplication.run(TecnicaApplication.class, args);
	}

}
