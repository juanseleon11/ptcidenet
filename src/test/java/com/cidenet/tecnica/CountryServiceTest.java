package com.cidenet.tecnica;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import com.cidenet.tecnica.model.Country;
import com.cidenet.tecnica.repository.CountryRepository;
import com.cidenet.tecnica.rest.exceptions.NoCountriesException;
import com.cidenet.tecnica.rest.exceptions.NonExistentCountryException;
import com.cidenet.tecnica.service.CountryService;




@SpringBootTest
class CountryServiceTest {

	@Mock
	private CountryRepository countryRepo;
	
    @InjectMocks
    private CountryService countryServ;
       
	@Test
	void whenEmptyCountryList_thenNoCountriesExceptionShouldFire() {
		
		when(countryRepo.findAll()).thenReturn(new ArrayList<>());
		
		Exception exception = assertThrows(NoCountriesException.class, () -> {
			countryServ.getAllCountries();
	    });

	    String expectedMessage = "None countries were found.";
	    String actualMessage = exception.getMessage();
	    assertTrue(actualMessage.equals(expectedMessage));
	}
	
	
	@Test
	void whenInvalidCountryName_thenNoneCountryExceptionShouldFire() {
		
		when(countryRepo.findByName("mockArea")).thenReturn(Optional.empty());
		
		Exception exception = assertThrows(NonExistentCountryException.class, () -> {
			String mockCountry = "mockCountry";
			countryServ.searchCountry(mockCountry);
	    });

	    String expectedMessage = "Country with name mockCountry does not exists.";
	    String actualMessage = exception.getMessage();
	    assertTrue(actualMessage.equals(expectedMessage));    
	}
	
	@Test
	void whenValidCountryName_thenCountryShouldbeFound() {
		String mockCountry = "mockCountry";
		Country expectedCountry = new Country(); 
		expectedCountry.setName(mockCountry);
		when(countryRepo.findByName("mockCountry")).thenReturn(Optional.of(expectedCountry));
		
		Country actualCountry = countryServ.searchCountry(mockCountry);
		 
	    assertTrue(actualCountry.getName().equals(expectedCountry.getName()));  
	}

}
