package com.cidenet.tecnica;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.ConstraintViolationException;
import javax.validation.Validation;
import javax.validation.Validator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.domain.PageRequest;

import com.cidenet.tecnica.model.Area;
import com.cidenet.tecnica.model.Country;
import com.cidenet.tecnica.model.Employee;
import com.cidenet.tecnica.model.IdentificationType;
import com.cidenet.tecnica.repository.CountryRepository;
import com.cidenet.tecnica.repository.EmployeeRepository;
import com.cidenet.tecnica.rest.exceptions.BadEmployeeSearchException;
import com.cidenet.tecnica.rest.exceptions.BadQueryParameterEmployeeSearchException;
import com.cidenet.tecnica.rest.exceptions.EmployeeAlreadyExistsException;
import com.cidenet.tecnica.rest.exceptions.InvalidInformationException;
import com.cidenet.tecnica.service.EmployeeService;

@ComponentScan("com.smartown.server")
@DataJpaTest
class EmployeeServiceTest {

	@Mock
	private EmployeeRepository employeeRepo;
	@Mock
	private CountryRepository countryRepo;
	private static Country col;
	private static Country us;
	private static Area ar1;

	
    @InjectMocks
    private EmployeeService employeeServ;
    
    @BeforeEach
    public void setup() {
    	Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
    	employeeServ.setValidator(validator);
    	col= new Country();
    	col.setName("Colombia");
    	col.setEmailDomain("cidenet.com.co");

        us = new Country();
        us.setName("USA");
        us.setEmailDomain("cidenet.com.us");

        
        ar1 = new Area();
        ar1.setName("Administración");
        when(employeeRepo.save(Mockito.any(Employee.class))).thenAnswer(i -> i.getArguments()[0]);

    }

    private Employee createTestEmployee(String scenario) {

    	Employee employee = new Employee();
    	employee.setName("PEDRO");
    	employee.setFirstLastName("RAMIREZ");
    	employee.setSecondLastName("CORTES");
    	employee.setAditionalNames("RAMIRO");
    	employee.setCountry(col);
    	employee.setIdentificacionType(IdentificationType.CC);
    	employee.setIdentification("HOla-123");
    	employee.setArea(ar1);
    	employee.setStartDate(LocalDate.now().minusDays(10));
    	switch(scenario) {
    	case "longName":
    		employee.setName("JOHN RAUL ALEXANDER PEDRO");
    		break;
    	case "longFirstLastName":
    		employee.setFirstLastName("HAMILTON SMITH SANTA CRUZ");
    		break;
    	case "invalidSecondLastName":
    		employee.setSecondLastName("Nuñez");
    		break;
    	case "invalidIdentification":
    		employee.setIdentification("?¡");
    		break;
    	case "longIdentification":
    		employee.setIdentification("111111111111111111111111111111111111111111");
    		break;
    	case "badStartDate":
    		employee.setStartDate(LocalDate.now().minusDays(100));
    		break;
    	case "pedro2":
    		employee.setIdentificacionType(IdentificationType.PE);
    		break;
    	case "pedroUSA":
    		employee.setIdentificacionType(IdentificationType.PASS);
    		employee.setCountry(us);
    		break;
    	case "compound":
    		employee.setFirstLastName("HAMILTON SMITH");
    		break;
    	case "futureStartDate":
    		employee.setStartDate(LocalDate.now().plusDays(100));
    		break;
    		default:
    			break;
    	}
    	
    	return employee;
    }
    
	@Test
	void whenInvalidEmployeeInformation_thenConstraintViolationExceptionShouldFire() {
		
		assertThrows(ConstraintViolationException.class, () -> {
			employeeServ.createNewEmployee(createTestEmployee("longName"));
	    });
		
		
		assertThrows(ConstraintViolationException.class, () -> {
			employeeServ.createNewEmployee(createTestEmployee("longFirstLastName"));
	    });
		
		
		assertThrows(ConstraintViolationException.class, () -> {
			employeeServ.createNewEmployee(createTestEmployee("invalidSecondLastName"));
	    });
		
		assertThrows(ConstraintViolationException.class, () -> {
			employeeServ.createNewEmployee(createTestEmployee("invalidIdentification"));
	    });
		
		assertThrows(ConstraintViolationException.class, () -> {
			employeeServ.createNewEmployee(createTestEmployee("longIdentification"));
	    });

	}
	
	
	@Test
	void whenRepeatedEmployeeID_thenInvalidInformationExceptionShouldFire() {
		assertThrows(EmployeeAlreadyExistsException.class, () -> {
			Employee pedro = employeeServ.createNewEmployee(createTestEmployee(""));
	        when(employeeRepo.findByIdentificationAndIdentificacionType(Mockito.anyString(),
	        		Mockito.any(IdentificationType.class))).thenReturn(Optional.of(pedro));
			employeeServ.createNewEmployee(createTestEmployee(""));
	    });
		
	}
	
	@Test
	void whenInvalidStartDateEmployeeInformation_thenInvalidInformationExceptionShouldFire() {
	assertThrows(InvalidInformationException.class, () -> {
			employeeServ.createNewEmployee(createTestEmployee("badStartDate"));
	    });
		
		
		assertThrows(InvalidInformationException.class, () -> {
			employeeServ.createNewEmployee(createTestEmployee("futureStartDate"));
	    });


	}
	
	@Test
	void whenValidEmployeeInformation_thenEmailShouldBeWellGenerated() {
		Employee pedro = employeeServ.createNewEmployee(createTestEmployee(""));

		assertTrue(pedro.getEmail().equals("pedro.ramirez@cidenet.com.co"));
}
	
	@Test
	void whenValidEmployeeInformationAndMultipleEmployeesWithSameName_thenEmailShouldBeWellGenerated() {
		
		Employee pedro = employeeServ.createNewEmployee(createTestEmployee(""));
		List<Employee> testList = new ArrayList<>();
		testList.add(pedro);
        when(employeeRepo.findByNameAndFirstLastNameAndCountry_id(Mockito.anyString(),
        		Mockito.anyString(), Mockito.anyInt())).thenReturn(Optional.of(testList));
		Employee pedro1 = employeeServ.createNewEmployee(createTestEmployee("pedro2"));
		

		assertTrue(pedro.getEmail().equals("pedro.ramirez@cidenet.com.co"));
		assertTrue(pedro1.getEmail().equals("pedro.ramirez.1@cidenet.com.co"));

	}
	
	@Test
	void whenValidEmployeeInformationAndMultipleEmployeesWithSameNameAndDiferentCountry_thenEmailShouldBeWellGenerated() {
		Employee pedro = employeeServ.createNewEmployee(createTestEmployee(""));
		List<Employee> testList = new ArrayList<>();
        when(employeeRepo.findByNameAndFirstLastNameAndCountry_id(Mockito.anyString(),
        		Mockito.anyString(), Mockito.anyInt())).thenReturn(Optional.of(testList));
		Employee pedrousa = employeeServ.createNewEmployee(createTestEmployee("pedroUSA"));
		System.out.println(pedro.getEmail());
		System.out.println(pedrousa.getEmail());
		assertTrue(pedro.getEmail().equals("pedro.ramirez@cidenet.com.co"));
		assertTrue(pedrousa.getEmail().equals("pedro.ramirez@cidenet.com.us"));

	}
	
	
	@Test
	void whenValidEmployeeInformationAndCompoundLastName_thenEmailShouldBeWellGenerated() {
		Employee pedroComp = employeeServ.createNewEmployee(createTestEmployee("compound"));
		assertTrue(pedroComp.getEmail().equals("pedro.hamiltonsmith@cidenet.com.co"));
	}
	
	
	@Test
	void whenUpdatingEmployeewithDiferentName_thenShouldHaveEmaiUpdated() {
			Employee pedro = employeeServ.createNewEmployee(createTestEmployee(""));
			assertTrue(pedro.getEmail().equals("pedro.ramirez@cidenet.com.co"));
	        when(employeeRepo.findByIdentificationAndIdentificacionType(Mockito.anyString(),
	        		Mockito.any(IdentificationType.class))).thenReturn(Optional.of(pedro));
	        Employee alejo  =createTestEmployee("");
	        alejo.setName("ALEJANDRO");
			Employee testAlejo = employeeServ.updateEmployee(alejo);
			assertTrue(testAlejo.getName().equals("ALEJANDRO"));
			assertTrue(testAlejo.getEmail().equals("alejandro.ramirez@cidenet.com.co"));
	}
	
	@Test
	void whenUpdatingNonExistingEmployee_thenUpdateSuccess() {
	        when(employeeRepo.findByIdentificationAndIdentificacionType(Mockito.anyString(),
	        		Mockito.any(IdentificationType.class))).thenReturn(Optional.empty());
	        Employee pedro = employeeServ.updateEmployee(createTestEmployee(""));
			assertTrue(pedro.getEmail().equals("pedro.ramirez@cidenet.com.co"));
	}
	
	@Test
	void whenInvalidEmployeeSearchCountry_thenShouldBadEmployeeSearchExceptionFire() {
        when(countryRepo.findByName(Mockito.anyString())).thenReturn(Optional.empty());
       Map<String, String> queryParams = new HashMap<>();
       queryParams.put("country", "Rusia");
       assertThrows(BadEmployeeSearchException.class, () -> {
			employeeServ.searchEmployees(PageRequest.of(1, 5), queryParams);
	    });
	}
	
	@Test
	void whenInvalidEmployeeSearchQueryParam_thenShouldBadQueryParameterEmployeeSearchExceptionFire() {
	       Map<String, String> queryParams = new HashMap<>();
	       queryParams.put("name", "Rusia");
	       queryParams.put("badParam", "123123");
	       queryParams.put("identification", "123123");
	     
		assertThrows(BadQueryParameterEmployeeSearchException.class, () -> {
			employeeServ.searchEmployees(PageRequest.of(1, 5), queryParams);
	    });
	}


}
