package com.cidenet.tecnica;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.internal.matchers.Any;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.cidenet.tecnica.model.Area;
import com.cidenet.tecnica.repository.AreaRepository;
import com.cidenet.tecnica.rest.AreaController;
import com.cidenet.tecnica.rest.exceptions.BadQueryParameterEmployeeSearchException;
import com.cidenet.tecnica.rest.exceptions.NoAreasException;
import com.cidenet.tecnica.rest.exceptions.NonExistentAreaException;
import com.cidenet.tecnica.service.AreaService;

@SpringBootTest
class AreaServiceTest {



	@Mock
	private AreaRepository areaRepo;
	
    @InjectMocks
    private AreaService areaServ;
       
	@Test
	void whenEmptyAreaList_thenNoAreasExceptionShouldFire() {
		
		when(areaRepo.findAll()).thenReturn(new ArrayList<>());
		
		Exception exception = assertThrows(NoAreasException.class, () -> {
			areaServ.getAllAreas();
	    });

	    String expectedMessage = "None areas were found.";
	    String actualMessage = exception.getMessage();
	    assertTrue(actualMessage.equals(expectedMessage));
	}
	
	
	@Test
	void whenInvalidAreaName_thenNoneAreaExceptionShouldFire() {
		
		when(areaRepo.findByName("mockArea")).thenReturn(Optional.empty());
		
		Exception exception = assertThrows(NonExistentAreaException.class, () -> {
			String mockArea = "mockArea";
			areaServ.searchArea(mockArea);
	    });

	    String expectedMessage = "Area with name mockArea does not exists.";
	    String actualMessage = exception.getMessage();
	    assertTrue(actualMessage.equals(expectedMessage));    
	}
	
	@Test
	void whenValidAreaName_thenAreaShouldbeFound() {
		String mockArea = "mockArea";
		Area expectedArea = new Area(); 
		expectedArea.setName(mockArea);
		when(areaRepo.findByName("mockArea")).thenReturn(Optional.of(expectedArea));
		
		
		Area actualArea = areaServ.searchArea(mockArea);
		 


	    assertTrue(actualArea.getName().equals(expectedArea.getName()));  
	}
	
	

}
